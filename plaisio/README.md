# Plaisio annoyances

Here is an explanation of each rule included in this filter:

## In a list of products
* `www.plaisio.gr##.badge`: In a list of products, if a products has a discount a badge is displayed on the corner of the product's image.
* `www.plaisio.gr##.productPrevPrice`: In a list of products, if a products has a discount, the original price is shown as well as the current one.

## Bottom of the page
* `www.plaisio.gr##.smallBottomUspItemsWrap`: A banner that says about free pickup from the store, free phone support etc etc
* `www.plaisio.gr##footer > .top.row`: A colorful banner that shows the phone number.
* `www.plaisio.gr##footer > .middle.row`: A form to sign up for the newsletter
* `www.plaisio.gr##footer > .bottom.row`: The footer
