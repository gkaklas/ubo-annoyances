# gkaklas uBo Annoyances

Various adblock filters I created to make my experience a bit better or productive on various websites.

* StackExchange Annoyances: [README](https://gitlab.com/gkaklas/ubo-annoyances/blob/master/stackexchange/README.md), [filter link](abp:subscribe?location=https%3A%2F%2Fgitlab.com%2Fgkaklas%2Fubo-annoyances%2Fraw%2Fmaster%2Fstackexchange%2Fubo-gkaklas-stackexchange-annoyances.txt&amp;title=gkaklas%20StackExchange%20Annoyances)
* AliExpress Annoyances: [README](https://gitlab.com/gkaklas/ubo-annoyances/blob/master/aliexpress/README.md), [filter link](abp:subscribe?location=https%3A%2F%2Fgitlab.com%2Fgkaklas%2Fubo-annoyances%2Fraw%2Fmaster%2Faliexpress%2Fubo-gkaklas-aliexpress-annoyances.txt&amp;title=gkaklas%20AliExpress%20Annoyances)
* Plaisio Annoyances: [README](https://gitlab.com/gkaklas/ubo-annoyances/blob/master/plaisio/README.md), [filter link](abp:subscribe?location=https%3A%2F%2Fgitlab.com%2Fgkaklas%2Fubo-annoyances%2Fraw%2Fmaster%2Fplaisio%2Fubo-gkaklas-plaisio-annoyances.txt&amp;title=gkaklas%20Plaisio%20Annoyances)

Personally I use and recommend the awesome [uBlock  Origin](https://github.com/gorhill/uBlock) (for [Firefox](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/) and [Chrome](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm)), so I've only tested this filters with uBlock Origin, but it should work with other adblock addons as well.
