# AliExpress annoyances
I got a bit tired of the marketing spam, flashing images, mobile app ads and other useless info the website shows, so I decided to write some rules that hide them. I recommend disabling these rules before doing something important (e.g. before checkout), since you' re never sure if they reuse some of the generic ids or classes to show important information.

Here is a list of the rules included, explaining which part of the page each one hides (and in some cases why I wanted to hide it):
## Global
Elements that are displayed on every page on the website.
* `www.aliexpress.com##.ui-fixed-panel`: The floating button on the bottom right, with a link for contacting the owners.
* `www.aliexpress.com##.show-history`: The floating button on the bottom right, which opens a dialog that shows your recently viewed products (if you're logged in?).

## Product page
* `www.aliexpress.com##.product-price-original`: If the product has a discount, the original price is shown greyed and striked out above the current price in a smaller font. Personally, since most offline and online stores lie about the original price to make the discount seem better, I only judge from the current price of the product.
* `www.aliexpress.com###j-related-products`: On the bottom of the page, the "More products" section shows other products "From this seller", "From other sellers" and "Premium related products". Personally I do product research independent of the market's recommendations, so I don't need it.
* `www.aliexpress.com###j-transaction-feedback`: On the bottom of the page. I just don't get why would someone want to know about how many pieces of the product a stranger bought, their country and the date and time they bought it.
* `www.aliexpress.com##.clearfix.buyer-pretection-wrap`: Below the "Add to cart" button, a small banner reminding of the default 60-Day buyer protection

## List of products
E.g. viewing a category, search results etc

### General
* `www.aliexpress.com###survey-container`: Bottom of the page, the box that asks you "Did you find what you were looking for?".
* `www.aliexpress.com###p4pHotProducts-old`: "Premium Related Products" on the bottom of the page.
* `www.aliexpress.com##.bp-horizontal-banner`: Buyer protection banner on the bottom of the page.
* `www.aliexpress.com###seo-category-teletext`: Bottom of the page, an article saying a few words about the product in general. For example, while browsing the T-shirt category it explains the different types of T-shirts; sometimes on custom search results it's 7 paragraphs of "we have the best {searchQuery}, choose us"

### Left sidebar
* `www.aliexpress.com##.mobile-app.me-ui-box`: Advertisement on the left sidebar with a QR code to download the application. If I don't want to download the app, the ad is annoying. If I did want to download the app, I would just search for it on my phone's marketplace, then I wouldn't need the ad anymore. So 99% of the time this ad is useless.
* `www.aliexpress.com###historyOuterBox`: On the left sidebar, a box which shows your recently viewed products.

### Product-specific
* `www.aliexpress.com##.original-price`: Sometimes if a product has a discount, the original price is shown under the product.
* `www.aliexpress.com##.new-discount-rate`: Same as the above, showing the discount rate.
