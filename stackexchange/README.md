# StackExchange Annoyances

Rules for removing elements on SE sites so I can focus on the content I want to read about: questions and answers. When I want to participate in the community I disable these filters, but when I'm solving a problem I want to focus on the answer, so I need the UI to be as minimal as possible.

## General
* `stackexchange.com###footer`:  The footer.
* `stackexchange.com##.bottom-notice`: On the bottom of the page: "Not the answer you're looking for? Browse other questions tagged...".
* `stackexchange.com##.js-dismissable-hero.s-hero__dark.s-hero`: The large banner on top of the page that reminds you to sign up.

## Sidebars
* `stackexchange.com###left-sidebar`: Well, the left sidebar
* `stackexchange.com###chat-feature`: On the right sidebar, "xyz people chatting"
* `stackexchange.com###hot-network-questions`:  Hot network questions section on the right sidebar.

## Per-entry information
* `stackexchange.com##.comments`: Comments on the question and answers. Not that they're not useful, but usually the thing you're looking for is in the accepted answer. If it's not, I will usually start reading the next answers so I want to focus my attention on those.
* `stackexchange.com##.comments-link`: "Add a comment" link. 

## Bottom of the page
* `stackexchange.com##.new-post-login.form-item`: If you are not logged in, there is a form on the bottom of the page that asks you to log in.
* `stackexchange.com###post-form`: The form that lets you post an answer. I removed this because at least for me, I spend most of the time reading answers than posting them, and I can always temporarily disable these rules when I want to post an answer.

