#!/bin/env sh

sites=(
    "stackexchange.com"
    "stackoverflow.com"
    "superuser.com"
    "askubuntu.com"
    "mathoverflow.net"
    "serverfault.com"
    "stackapps.com"
)

rules=(
    "###left-sidebar"
    "###chat-feature"
    "###announcement-banner"
    "###hot-network-questions"
    "##.new-post-login.form-item"
    "###post-form"
    "##.comments"
    "##.comments-link"
    "###footer"
    "###js-gdpr-consent-banner"
    "##.js-dismissable-hero._fixed.hero-container.old-hero"
    "##.bottom-notice"
    "##.js-dismissable-hero.s-hero__dark.s-hero"
)

for site in "${sites[@]}";do
    for rule in "${rules[@]}";do
       echo "$site$rule"
    done
done
